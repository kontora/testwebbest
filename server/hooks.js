const {rand, sleep, db_get} = require('../functions');

/**
 * @param {{email: string, pwd: string}} data
 */

async function login(data) {
    console.log('login info:', data.email, data.pwd);



    const user = await db_get("SELECT * FROM users WHERE email = $email AND passw = $passw LIMIT 1", {
        $email: data.email,
        $passw: data.pwd,
    });

     if (user) {
        return {
            status: 'ok',
            user_id: parseInt(user.user_id),
        }
    }

    return {
        status: 'error',
        error: 'WRONG_PWD',
    };

}


/**
 * @param {{ts: number}} data
 */
async function ping(data) {
    await sleep(rand(100, 3000));

    return {
        latency: +new Date() - data.ts,
    };
}

module.exports = {
    login,
    ping,
};