const hooks = require('./hooks');
const {db_create} = require('./../functions');

const {get_args, mute, socket_bind, async_zmq} = require('../functions');

const {pub_port, sub_port} = get_args(3000, 3001);

async function main() {
    db_create();
    const [pub_sock, sub_sock] = await Promise.all([
        socket_bind(pub_port, 'pub'),
        socket_bind(sub_port, 'sub'),
    ]);

    sub_sock.subscribe('api_in');
    console.log('Initialization...');
    async_zmq(pub_sock, sub_sock, 'api_in', 'api_out', hooks);

    console.log('Server is listening on ' + pub_port + ', ' + sub_port);
    mute();
}


main();
