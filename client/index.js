const hooks = require('./hooks');

const {get_args, rand_str, input, mute, socket_open, async_zmq, sleep} = require('../functions');

const {pub_port, sub_port} = get_args(3001, 3000);


async function main() {
    try {
        const [pub_sock, sub_sock] = await Promise.all([
            socket_open(pub_port, 'pub'),
            socket_open(sub_port, 'sub'),
        ]);

        sub_sock.subscribe('api_out');
        console.log('Initialization...');
        async_zmq(pub_sock, sub_sock, 'api_out', 'api_in', hooks);

        console.log('Hello, please login to proceed');

        let auth = false;

        while (!auth) {
            const email = await input('email: ');
            const pwd = await input('password: ', true);

            mute(true);
            const data = await pub_sock.send({
                type: 'login',
                email,
                pwd,
            });
            mute(false);

            auth = data.status === 'ok';

            if (data.status === 'error' && data.error) {
                console.log(data.error);
            }
        }

        console.log('ok');

        while (true) {
            const data = await pub_sock.send({
                type: 'ping',
                ts: +new Date(),
            });

            if (data) {
                console.log('[ping] latency:', data.latency);
            }

            await sleep(1000);
        }

    } catch (e) {
        console.log(e);
        console.log('Server is unavailable');
        process.exit(1);
    }
}


main();
