const zmq = require('zeromq');
const readline = require('readline');
const WriteableStream = require('stream').Writable;
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(__dirname + '/db.sqlite');
const util = require('util');


const rl = readline.createInterface({
    input: process.stdin,
    output: new WriteableStream({
        write(chunk, encoding, callback) {
            if (!rl.hidden) {
                process.stdout.write(chunk, encoding);
            }
            callback();
        },
    }),
    terminal: true,
});


/* Common helpers */
function async_zmq(pub_sock, sub_sock, in_channel, out_channel, hooks) {

    const MSG_ID_LEN = 32;

    let queue = {};

    sub_sock.on('message', async (channel, buffer) => {
        if (in_channel !== channel.toString()) {
            return;
        }

        let data;

        try {
            data = JSON.parse(buffer.toString())
        } catch (e) {
            return;
        }

        if (typeof data.msg_id !== 'string' || data.msg_id.length !== MSG_ID_LEN) {
            return;
        }

        const msg_id = data.msg_id;
        const type = data.type;
        delete data.msg_id;
        delete data.type;

        if (queue[msg_id]) {
            queue[msg_id](data);
        } else if (hooks.hasOwnProperty(type) && typeof hooks[type] === 'function') {
            const response = await hooks[type](data, pub_sock, sub_sock);
            if (response && typeof response === 'object') {
                response.type = response.type || type;
                response.msg_id = response.msg_id || msg_id;
                pub_sock.send(response, false).then(() => 0);
            }
        } else {
            console.error('Unknown type of package received "' + type + '"')
        }
    });

    const send = pub_sock.send.bind(pub_sock);
    pub_sock.send = async (data, wait_response = true, timeout = 15000) => {
        return new Promise((resolve, reject) => {
            const req_id = rand_str(MSG_ID_LEN);
            if (wait_response) {
                queue[req_id] = resolve;
                setTimeout(() => {
                    delete queue[req_id];
                    resolve(null);
                }, timeout);
            }

            data.msg_id = data.msg_id || req_id;

            send([out_channel, JSON.stringify(data)]);
        });
    };
}

async function sleep(timeout) {
    return new Promise(resolve => setTimeout(() => resolve(), timeout));
}

function str_chunk(str, size) {
    const numChunks = Math.ceil(str.length / size);
    const chunks = new Array(numChunks);

    for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
        chunks[i] = str.substr(o, size);
    }

    return chunks;
}

function rand(min, max) {
    if (typeof max === 'undefined' || max === null) {
        max = min;
        min = 0;
    }
    return Math.floor(min + Math.random() * (max + 1 - min));
}

function rand_str(len = 32) {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    let buffer = '';

    for (let i = 0; i < len; i++) {
        buffer += chars[rand(chars.length - 1)];
    }

    return buffer;
}

function get_args(default_pub = 3000, default_sub = 3001) {
    const pub_port = parseInt((process.argv[2] || default_pub));
    const sub_port = parseInt((process.argv[3] || default_sub));

    if (!pub_port || !sub_port || (pub_port < 1 || pub_port > 65535) || (sub_port < 1 || sub_port > 65535)) {
        if (!pub_port) {
            console.log('Invalid pub port ' + process.argv[2]);
        }

        if (!sub_port) {
            console.log('Invalid sub port ' + process.argv[3]);
        }

        console.log('Port must be in range 1-65535\n');
        console.log('Using:\n\tnode server.js [pub=3000] [sub=3001]');

        process.exit(1);
    }

    return {pub_port, sub_port};
}

/**
 * @return {Promise<string>}
 */
async function input(prefix = '', hidden = false) {
    return new Promise((r => {
        rl.question(prefix, data => {
            if (rl.hidden) {
                console.log('');
            }
            rl.hidden = false;
            r(data);
        });

        rl.hidden = hidden;
    }));
}

function mute(state = true) {
    rl.hidden = state;
}


/* Server helpers */

/**
 * @return {Promise<zmq.Socket>}
 */
function socket_bind(port, type, host = '127.0.0.1') {
    return new Promise(((resolve, reject) => {
        const sock = zmq.socket(type);
        sock.monitor();

        sock.on('listen', () => {
            resolve(sock);
        });

        try {
            sock.bindSync('tcp://' + host + ':' + port);
        } catch (e) {
            reject(new Error('Address ' + host + ':' + port + ' for ' + type + ' already in use'));
        }
    }));
}


/* Client helpers */

/**
 * @return {Promise<zmq.Socket>}
 */
async function socket_open(port, type, host = '127.0.0.1', timeout = 3000) {
    return new Promise((resolve, reject) => {
        const sock = zmq.socket(type);
        sock.monitor();

        const timeout_timer = setTimeout(() => {
            reject(new Error('Address ' + host + ':' + port + ' unreachable for type ' + type));
        }, timeout);

        sock.on('connect', () => {
            clearInterval(timeout_timer);
            resolve(sock);
        });

        sock.connect('tcp://' + host + ':' + port);
    });
}

function db_create() {
    db.serialize(function () {
        db.run("CREATE TABLE IF NOT EXISTS users (user_id INTEGER, email TEXT, passw TEXT)",);
        db.run(`INSERT INTO users (user_id, email, passw) VALUES(?,?,?)`, [1, 'test@test.com', 'test'], function (err) {
            if (err) {
                return console.log(err.message);
            }
            console.log(`Created DB...`);
        });
    });
}


module.exports = {
    /* common */
    async_zmq,
    get_args,
    input,
    mute,
    sleep,
    rand,
    rand_str,

    /* server */
    socket_bind,

    /* client */
    socket_open,

    /* create DB */
    db,
    db_get: util.promisify(db.get.bind(db)),
    db_create,
};