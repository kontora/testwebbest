const crypto = require('crypto')
const path = require('path')
const fs = require('fs')
const {writeFileSync} = require('fs')
const {generateKeyPairSync} = require('crypto')

function generateKeys() {
    const {privateKey, publicKey} = generateKeyPairSync('rsa', {
        modulusLength: 4096,
        publicKeyEncoding: {
            type: 'pkcs1',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs1',
            format: 'pem',
            cipher: 'aes-256-cbc',
            passphrase: '',
        },
    })


    return {privateKey, publicKey};
}

const {privateKey, publicKey} = generateKeys();

function encrypt(toEncrypt, relativeOrAbsolutePathToPublicKey) {
    // const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey)
    // const publicKey = fs.readFileSync(absolutePath, 'utf8')
    const buffer = Buffer.from(toEncrypt, 'utf8')
    const encrypted = crypto.publicEncrypt(publicKey, buffer)
    return encrypted.toString('base64')
}

function decrypt(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
    // const absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey)
    // const privateKey = fs.readFileSync(absolutePath, 'utf8')
    const buffer = Buffer.from(toDecrypt, 'base64')
    const decrypted = crypto.privateDecrypt(privateKey.toString(), buffer)
    return decrypted.toString('utf8')
}


const enc = encrypt('hello', `public.pem`)
console.log('enc', enc)

const dec = decrypt(enc, `private.pem`)
console.log('dec', dec)